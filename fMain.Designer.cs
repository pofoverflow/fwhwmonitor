﻿namespace FWHWproject
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.btnINFO = new System.Windows.Forms.Button();
            this.tableCPU = new System.Windows.Forms.TableLayoutPanel();
            this.btncfon = new System.Windows.Forms.Button();
            this.btncfoff = new System.Windows.Forms.Button();
            this.btncuoff = new System.Windows.Forms.Button();
            this.btncuon = new System.Windows.Forms.Button();
            this.lblcputemp = new System.Windows.Forms.Label();
            this.lblcpupower = new System.Windows.Forms.Label();
            this.btncpon = new System.Windows.Forms.Button();
            this.lblcpuusage = new System.Windows.Forms.Label();
            this.lblcpufreq = new System.Windows.Forms.Label();
            this.btncton = new System.Windows.Forms.Button();
            this.btnctoff = new System.Windows.Forms.Button();
            this.btncpoff = new System.Windows.Forms.Button();
            this.lblCPU = new System.Windows.Forms.Label();
            this.lblRAM = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblramLoad = new System.Windows.Forms.Label();
            this.lblramusage = new System.Windows.Forms.Label();
            this.btnruon = new System.Windows.Forms.Button();
            this.btnrlon = new System.Windows.Forms.Button();
            this.btnrloff = new System.Windows.Forms.Button();
            this.btnruoff = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnvramfoff = new System.Windows.Forms.Button();
            this.btngfoff = new System.Windows.Forms.Button();
            this.btnvramfon = new System.Windows.Forms.Button();
            this.btnglon = new System.Windows.Forms.Button();
            this.lblfVRAM = new System.Windows.Forms.Label();
            this.btngfon = new System.Windows.Forms.Button();
            this.btngloff = new System.Windows.Forms.Button();
            this.lblfGPU = new System.Windows.Forms.Label();
            this.lbltGPU = new System.Windows.Forms.Label();
            this.lblpGPU = new System.Windows.Forms.Label();
            this.btngpon = new System.Windows.Forms.Button();
            this.lbllGPU = new System.Windows.Forms.Label();
            this.btngton = new System.Windows.Forms.Button();
            this.btngtoff = new System.Windows.Forms.Button();
            this.btngpoff = new System.Windows.Forms.Button();
            this.btnCloseALL = new System.Windows.Forms.Button();
            this.timerData = new System.Windows.Forms.Timer(this.components);
            this.tableCPU.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnINFO
            // 
            this.btnINFO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnINFO.Font = new System.Drawing.Font("Razed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnINFO.ForeColor = System.Drawing.Color.LimeGreen;
            this.btnINFO.Location = new System.Drawing.Point(177, 599);
            this.btnINFO.Name = "btnINFO";
            this.btnINFO.Size = new System.Drawing.Size(89, 37);
            this.btnINFO.TabIndex = 3;
            this.btnINFO.Text = "info";
            this.btnINFO.UseVisualStyleBackColor = true;
            this.btnINFO.Click += new System.EventHandler(this.btnINFO_Click);
            // 
            // tableCPU
            // 
            this.tableCPU.ColumnCount = 3;
            this.tableCPU.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableCPU.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableCPU.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableCPU.Controls.Add(this.btncfon, 1, 3);
            this.tableCPU.Controls.Add(this.btncfoff, 2, 3);
            this.tableCPU.Controls.Add(this.btncuoff, 2, 2);
            this.tableCPU.Controls.Add(this.btncuon, 1, 2);
            this.tableCPU.Controls.Add(this.lblcputemp, 0, 0);
            this.tableCPU.Controls.Add(this.lblcpupower, 0, 1);
            this.tableCPU.Controls.Add(this.btncpon, 1, 1);
            this.tableCPU.Controls.Add(this.lblcpuusage, 0, 2);
            this.tableCPU.Controls.Add(this.lblcpufreq, 0, 3);
            this.tableCPU.Controls.Add(this.btncton, 1, 0);
            this.tableCPU.Controls.Add(this.btnctoff, 2, 0);
            this.tableCPU.Controls.Add(this.btncpoff, 2, 1);
            this.tableCPU.Location = new System.Drawing.Point(12, 60);
            this.tableCPU.Name = "tableCPU";
            this.tableCPU.RowCount = 4;
            this.tableCPU.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.57143F));
            this.tableCPU.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.42857F));
            this.tableCPU.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableCPU.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableCPU.Size = new System.Drawing.Size(228, 147);
            this.tableCPU.TabIndex = 0;
            // 
            // btncfon
            // 
            this.btncfon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncfon.ForeColor = System.Drawing.Color.Lime;
            this.btncfon.Location = new System.Drawing.Point(106, 110);
            this.btncfon.Name = "btncfon";
            this.btncfon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btncfon.Size = new System.Drawing.Size(39, 28);
            this.btncfon.TabIndex = 6;
            this.btncfon.Text = "ON";
            this.btncfon.UseVisualStyleBackColor = true;
            this.btncfon.Click += new System.EventHandler(this.btncfon_Click);
            // 
            // btncfoff
            // 
            this.btncfoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncfoff.ForeColor = System.Drawing.Color.Lime;
            this.btncfoff.Location = new System.Drawing.Point(151, 110);
            this.btncfoff.Name = "btncfoff";
            this.btncfoff.Size = new System.Drawing.Size(39, 28);
            this.btncfoff.TabIndex = 7;
            this.btncfoff.Text = "OFF";
            this.btncfoff.UseVisualStyleBackColor = true;
            this.btncfoff.Click += new System.EventHandler(this.btncfoff_Click);
            // 
            // btncuoff
            // 
            this.btncuoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncuoff.ForeColor = System.Drawing.Color.Lime;
            this.btncuoff.Location = new System.Drawing.Point(151, 73);
            this.btncuoff.Name = "btncuoff";
            this.btncuoff.Size = new System.Drawing.Size(39, 28);
            this.btncuoff.TabIndex = 11;
            this.btncuoff.Text = "OFF";
            this.btncuoff.UseVisualStyleBackColor = true;
            this.btncuoff.Click += new System.EventHandler(this.btncuoff_Click);
            // 
            // btncuon
            // 
            this.btncuon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncuon.ForeColor = System.Drawing.Color.Lime;
            this.btncuon.Location = new System.Drawing.Point(106, 73);
            this.btncuon.Name = "btncuon";
            this.btncuon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btncuon.Size = new System.Drawing.Size(39, 28);
            this.btncuon.TabIndex = 10;
            this.btncuon.Text = "ON";
            this.btncuon.UseVisualStyleBackColor = true;
            this.btncuon.Click += new System.EventHandler(this.btncuon_Click);
            // 
            // lblcputemp
            // 
            this.lblcputemp.AutoSize = true;
            this.lblcputemp.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcputemp.ForeColor = System.Drawing.Color.Lime;
            this.lblcputemp.Location = new System.Drawing.Point(3, 0);
            this.lblcputemp.Name = "lblcputemp";
            this.lblcputemp.Size = new System.Drawing.Size(73, 32);
            this.lblcputemp.TabIndex = 0;
            this.lblcputemp.Text = "temp";
            // 
            // lblcpupower
            // 
            this.lblcpupower.AutoSize = true;
            this.lblcpupower.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpupower.ForeColor = System.Drawing.Color.Lime;
            this.lblcpupower.Location = new System.Drawing.Point(3, 34);
            this.lblcpupower.Name = "lblcpupower";
            this.lblcpupower.Size = new System.Drawing.Size(97, 32);
            this.lblcpupower.TabIndex = 1;
            this.lblcpupower.Text = "power";
            // 
            // btncpon
            // 
            this.btncpon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncpon.ForeColor = System.Drawing.Color.Lime;
            this.btncpon.Location = new System.Drawing.Point(106, 37);
            this.btncpon.Name = "btncpon";
            this.btncpon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btncpon.Size = new System.Drawing.Size(39, 28);
            this.btncpon.TabIndex = 8;
            this.btncpon.Text = "ON";
            this.btncpon.UseVisualStyleBackColor = true;
            this.btncpon.Click += new System.EventHandler(this.btncpon_Click);
            // 
            // lblcpuusage
            // 
            this.lblcpuusage.AutoSize = true;
            this.lblcpuusage.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpuusage.ForeColor = System.Drawing.Color.Lime;
            this.lblcpuusage.Location = new System.Drawing.Point(3, 70);
            this.lblcpuusage.Name = "lblcpuusage";
            this.lblcpuusage.Size = new System.Drawing.Size(87, 32);
            this.lblcpuusage.TabIndex = 2;
            this.lblcpuusage.Text = "load";
            // 
            // lblcpufreq
            // 
            this.lblcpufreq.AutoSize = true;
            this.lblcpufreq.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpufreq.ForeColor = System.Drawing.Color.Lime;
            this.lblcpufreq.Location = new System.Drawing.Point(3, 107);
            this.lblcpufreq.Name = "lblcpufreq";
            this.lblcpufreq.Size = new System.Drawing.Size(77, 32);
            this.lblcpufreq.TabIndex = 3;
            this.lblcpufreq.Text = "freq";
            // 
            // btncton
            // 
            this.btncton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncton.ForeColor = System.Drawing.Color.Lime;
            this.btncton.Location = new System.Drawing.Point(106, 3);
            this.btncton.Name = "btncton";
            this.btncton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btncton.Size = new System.Drawing.Size(39, 28);
            this.btncton.TabIndex = 4;
            this.btncton.Text = "ON";
            this.btncton.UseVisualStyleBackColor = true;
            this.btncton.Click += new System.EventHandler(this.btncton_Click);
            // 
            // btnctoff
            // 
            this.btnctoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnctoff.ForeColor = System.Drawing.Color.Lime;
            this.btnctoff.Location = new System.Drawing.Point(151, 3);
            this.btnctoff.Name = "btnctoff";
            this.btnctoff.Size = new System.Drawing.Size(39, 28);
            this.btnctoff.TabIndex = 5;
            this.btnctoff.Text = "OFF";
            this.btnctoff.UseVisualStyleBackColor = true;
            this.btnctoff.Click += new System.EventHandler(this.btnctoff_Click);
            // 
            // btncpoff
            // 
            this.btncpoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncpoff.ForeColor = System.Drawing.Color.Lime;
            this.btncpoff.Location = new System.Drawing.Point(151, 37);
            this.btncpoff.Name = "btncpoff";
            this.btncpoff.Size = new System.Drawing.Size(39, 28);
            this.btncpoff.TabIndex = 9;
            this.btncpoff.Text = "OFF";
            this.btncpoff.UseVisualStyleBackColor = true;
            this.btncpoff.Click += new System.EventHandler(this.btncpoff_Click);
            // 
            // lblCPU
            // 
            this.lblCPU.AutoSize = true;
            this.lblCPU.Font = new System.Drawing.Font("Razed", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPU.ForeColor = System.Drawing.Color.Lime;
            this.lblCPU.Location = new System.Drawing.Point(2, 0);
            this.lblCPU.Name = "lblCPU";
            this.lblCPU.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCPU.Size = new System.Drawing.Size(118, 57);
            this.lblCPU.TabIndex = 4;
            this.lblCPU.Text = "cpu";
            // 
            // lblRAM
            // 
            this.lblRAM.AutoSize = true;
            this.lblRAM.Font = new System.Drawing.Font("Razed", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRAM.ForeColor = System.Drawing.Color.Lime;
            this.lblRAM.Location = new System.Drawing.Point(2, 210);
            this.lblRAM.Name = "lblRAM";
            this.lblRAM.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblRAM.Size = new System.Drawing.Size(126, 57);
            this.lblRAM.TabIndex = 6;
            this.lblRAM.Text = "ram";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.lblramLoad, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblramusage, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnruon, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnrlon, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnrloff, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnruoff, 2, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(11, 270);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.57143F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.42857F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(228, 76);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // lblramLoad
            // 
            this.lblramLoad.AutoSize = true;
            this.lblramLoad.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblramLoad.ForeColor = System.Drawing.Color.Lime;
            this.lblramLoad.Location = new System.Drawing.Point(3, 0);
            this.lblramLoad.Name = "lblramLoad";
            this.lblramLoad.Size = new System.Drawing.Size(87, 32);
            this.lblramLoad.TabIndex = 0;
            this.lblramLoad.Text = "load";
            // 
            // lblramusage
            // 
            this.lblramusage.AutoSize = true;
            this.lblramusage.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblramusage.ForeColor = System.Drawing.Color.Lime;
            this.lblramusage.Location = new System.Drawing.Point(3, 36);
            this.lblramusage.Name = "lblramusage";
            this.lblramusage.Size = new System.Drawing.Size(105, 32);
            this.lblramusage.TabIndex = 1;
            this.lblramusage.Text = "usage";
            // 
            // btnruon
            // 
            this.btnruon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnruon.ForeColor = System.Drawing.Color.Lime;
            this.btnruon.Location = new System.Drawing.Point(114, 39);
            this.btnruon.Name = "btnruon";
            this.btnruon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnruon.Size = new System.Drawing.Size(39, 28);
            this.btnruon.TabIndex = 8;
            this.btnruon.Text = "ON";
            this.btnruon.UseVisualStyleBackColor = true;
            this.btnruon.Click += new System.EventHandler(this.btnruon_Click);
            // 
            // btnrlon
            // 
            this.btnrlon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnrlon.ForeColor = System.Drawing.Color.Lime;
            this.btnrlon.Location = new System.Drawing.Point(114, 3);
            this.btnrlon.Name = "btnrlon";
            this.btnrlon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnrlon.Size = new System.Drawing.Size(39, 28);
            this.btnrlon.TabIndex = 4;
            this.btnrlon.Text = "ON";
            this.btnrlon.UseVisualStyleBackColor = true;
            this.btnrlon.Click += new System.EventHandler(this.btnrlon_Click);
            // 
            // btnrloff
            // 
            this.btnrloff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnrloff.ForeColor = System.Drawing.Color.Lime;
            this.btnrloff.Location = new System.Drawing.Point(159, 3);
            this.btnrloff.Name = "btnrloff";
            this.btnrloff.Size = new System.Drawing.Size(39, 28);
            this.btnrloff.TabIndex = 5;
            this.btnrloff.Text = "OFF";
            this.btnrloff.UseVisualStyleBackColor = true;
            this.btnrloff.Click += new System.EventHandler(this.btnrloff_Click);
            // 
            // btnruoff
            // 
            this.btnruoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnruoff.ForeColor = System.Drawing.Color.Lime;
            this.btnruoff.Location = new System.Drawing.Point(159, 39);
            this.btnruoff.Name = "btnruoff";
            this.btnruoff.Size = new System.Drawing.Size(39, 28);
            this.btnruoff.TabIndex = 9;
            this.btnruoff.Text = "OFF";
            this.btnruoff.UseVisualStyleBackColor = true;
            this.btnruoff.Click += new System.EventHandler(this.btnruoff_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Razed", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(2, 349);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(121, 57);
            this.label1.TabIndex = 8;
            this.label1.Text = "gpu";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.btnvramfoff, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.btngfoff, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnvramfon, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnglon, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblfVRAM, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btngfon, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.btngloff, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblfGPU, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lbltGPU, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblpGPU, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btngpon, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbllGPU, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btngton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btngtoff, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btngpoff, 2, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(12, 409);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(254, 174);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // btnvramfoff
            // 
            this.btnvramfoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnvramfoff.ForeColor = System.Drawing.Color.Lime;
            this.btnvramfoff.Location = new System.Drawing.Point(210, 139);
            this.btnvramfoff.Name = "btnvramfoff";
            this.btnvramfoff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnvramfoff.Size = new System.Drawing.Size(39, 28);
            this.btnvramfoff.TabIndex = 10;
            this.btnvramfoff.Text = "OFF";
            this.btnvramfoff.UseVisualStyleBackColor = true;
            this.btnvramfoff.Click += new System.EventHandler(this.btnvramfoff_Click);
            // 
            // btngfoff
            // 
            this.btngfoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngfoff.ForeColor = System.Drawing.Color.Lime;
            this.btngfoff.Location = new System.Drawing.Point(210, 105);
            this.btngfoff.Name = "btngfoff";
            this.btngfoff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btngfoff.Size = new System.Drawing.Size(39, 28);
            this.btngfoff.TabIndex = 10;
            this.btngfoff.Text = "OFF";
            this.btngfoff.UseVisualStyleBackColor = true;
            this.btngfoff.Click += new System.EventHandler(this.btngfoff_Click);
            // 
            // btnvramfon
            // 
            this.btnvramfon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnvramfon.ForeColor = System.Drawing.Color.Lime;
            this.btnvramfon.Location = new System.Drawing.Point(165, 139);
            this.btnvramfon.Name = "btnvramfon";
            this.btnvramfon.Size = new System.Drawing.Size(39, 28);
            this.btnvramfon.TabIndex = 11;
            this.btnvramfon.Text = "ON";
            this.btnvramfon.UseVisualStyleBackColor = true;
            this.btnvramfon.Click += new System.EventHandler(this.btnvramfon_Click);
            // 
            // btnglon
            // 
            this.btnglon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnglon.ForeColor = System.Drawing.Color.Lime;
            this.btnglon.Location = new System.Drawing.Point(165, 71);
            this.btnglon.Name = "btnglon";
            this.btnglon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnglon.Size = new System.Drawing.Size(39, 28);
            this.btnglon.TabIndex = 6;
            this.btnglon.Text = "ON";
            this.btnglon.UseVisualStyleBackColor = true;
            this.btnglon.Click += new System.EventHandler(this.btnglon_Click);
            // 
            // lblfVRAM
            // 
            this.lblfVRAM.AutoSize = true;
            this.lblfVRAM.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfVRAM.ForeColor = System.Drawing.Color.Lime;
            this.lblfVRAM.Location = new System.Drawing.Point(3, 136);
            this.lblfVRAM.Name = "lblfVRAM";
            this.lblfVRAM.Size = new System.Drawing.Size(156, 32);
            this.lblfVRAM.TabIndex = 9;
            this.lblfVRAM.Text = "vram freq";
            // 
            // btngfon
            // 
            this.btngfon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngfon.ForeColor = System.Drawing.Color.Lime;
            this.btngfon.Location = new System.Drawing.Point(165, 105);
            this.btngfon.Name = "btngfon";
            this.btngfon.Size = new System.Drawing.Size(39, 28);
            this.btngfon.TabIndex = 11;
            this.btngfon.Text = "ON";
            this.btngfon.UseVisualStyleBackColor = true;
            this.btngfon.Click += new System.EventHandler(this.btngfon_Click);
            // 
            // btngloff
            // 
            this.btngloff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngloff.ForeColor = System.Drawing.Color.Lime;
            this.btngloff.Location = new System.Drawing.Point(210, 71);
            this.btngloff.Name = "btngloff";
            this.btngloff.Size = new System.Drawing.Size(39, 28);
            this.btngloff.TabIndex = 7;
            this.btngloff.Text = "OFF";
            this.btngloff.UseVisualStyleBackColor = true;
            this.btngloff.Click += new System.EventHandler(this.btngloff_Click);
            // 
            // lblfGPU
            // 
            this.lblfGPU.AutoSize = true;
            this.lblfGPU.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfGPU.ForeColor = System.Drawing.Color.Lime;
            this.lblfGPU.Location = new System.Drawing.Point(3, 102);
            this.lblfGPU.Name = "lblfGPU";
            this.lblfGPU.Size = new System.Drawing.Size(138, 32);
            this.lblfGPU.TabIndex = 9;
            this.lblfGPU.Text = "gpu freq";
            // 
            // lbltGPU
            // 
            this.lbltGPU.AutoSize = true;
            this.lbltGPU.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltGPU.ForeColor = System.Drawing.Color.Lime;
            this.lbltGPU.Location = new System.Drawing.Point(3, 0);
            this.lbltGPU.Name = "lbltGPU";
            this.lbltGPU.Size = new System.Drawing.Size(73, 32);
            this.lbltGPU.TabIndex = 0;
            this.lbltGPU.Text = "temp";
            // 
            // lblpGPU
            // 
            this.lblpGPU.AutoSize = true;
            this.lblpGPU.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpGPU.ForeColor = System.Drawing.Color.Lime;
            this.lblpGPU.Location = new System.Drawing.Point(3, 34);
            this.lblpGPU.Name = "lblpGPU";
            this.lblpGPU.Size = new System.Drawing.Size(97, 32);
            this.lblpGPU.TabIndex = 1;
            this.lblpGPU.Text = "power";
            // 
            // btngpon
            // 
            this.btngpon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngpon.ForeColor = System.Drawing.Color.Lime;
            this.btngpon.Location = new System.Drawing.Point(165, 37);
            this.btngpon.Name = "btngpon";
            this.btngpon.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btngpon.Size = new System.Drawing.Size(39, 28);
            this.btngpon.TabIndex = 8;
            this.btngpon.Text = "ON";
            this.btngpon.UseVisualStyleBackColor = true;
            this.btngpon.Click += new System.EventHandler(this.btngpon_Click);
            // 
            // lbllGPU
            // 
            this.lbllGPU.AutoSize = true;
            this.lbllGPU.Font = new System.Drawing.Font("Razed", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllGPU.ForeColor = System.Drawing.Color.Lime;
            this.lbllGPU.Location = new System.Drawing.Point(3, 68);
            this.lbllGPU.Name = "lbllGPU";
            this.lbllGPU.Size = new System.Drawing.Size(87, 32);
            this.lbllGPU.TabIndex = 3;
            this.lbllGPU.Text = "load";
            // 
            // btngton
            // 
            this.btngton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngton.ForeColor = System.Drawing.Color.Lime;
            this.btngton.Location = new System.Drawing.Point(165, 3);
            this.btngton.Name = "btngton";
            this.btngton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btngton.Size = new System.Drawing.Size(39, 28);
            this.btngton.TabIndex = 4;
            this.btngton.Text = "ON";
            this.btngton.UseVisualStyleBackColor = true;
            this.btngton.Click += new System.EventHandler(this.btngton_Click);
            // 
            // btngtoff
            // 
            this.btngtoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngtoff.ForeColor = System.Drawing.Color.Lime;
            this.btngtoff.Location = new System.Drawing.Point(210, 3);
            this.btngtoff.Name = "btngtoff";
            this.btngtoff.Size = new System.Drawing.Size(39, 28);
            this.btngtoff.TabIndex = 5;
            this.btngtoff.Text = "OFF";
            this.btngtoff.UseVisualStyleBackColor = true;
            this.btngtoff.Click += new System.EventHandler(this.btngtoff_Click);
            // 
            // btngpoff
            // 
            this.btngpoff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btngpoff.ForeColor = System.Drawing.Color.Lime;
            this.btngpoff.Location = new System.Drawing.Point(210, 37);
            this.btngpoff.Name = "btngpoff";
            this.btngpoff.Size = new System.Drawing.Size(39, 28);
            this.btngpoff.TabIndex = 9;
            this.btngpoff.Text = "OFF";
            this.btngpoff.UseVisualStyleBackColor = true;
            this.btngpoff.Click += new System.EventHandler(this.btngpoff_Click);
            // 
            // btnCloseALL
            // 
            this.btnCloseALL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseALL.Font = new System.Drawing.Font("Razed", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCloseALL.ForeColor = System.Drawing.Color.LimeGreen;
            this.btnCloseALL.Location = new System.Drawing.Point(12, 599);
            this.btnCloseALL.Name = "btnCloseALL";
            this.btnCloseALL.Size = new System.Drawing.Size(141, 37);
            this.btnCloseALL.TabIndex = 9;
            this.btnCloseALL.Text = "close all";
            this.btnCloseALL.UseVisualStyleBackColor = true;
            this.btnCloseALL.Click += new System.EventHandler(this.btnCloseALL_Click);
            // 
            // timerData
            // 
            this.timerData.Interval = 1000;
            this.timerData.Tick += new System.EventHandler(this.timerData_Tick);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(278, 648);
            this.Controls.Add(this.btnCloseALL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.lblRAM);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.lblCPU);
            this.Controls.Add(this.tableCPU);
            this.Controls.Add(this.btnINFO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.Text = "ControlCenter";
            this.Load += new System.EventHandler(this.fMain_Load);
            this.tableCPU.ResumeLayout(false);
            this.tableCPU.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnINFO;
        private System.Windows.Forms.TableLayoutPanel tableCPU;
        private System.Windows.Forms.Label lblcputemp;
        private System.Windows.Forms.Label lblcpupower;
        private System.Windows.Forms.Label lblcpuusage;
        private System.Windows.Forms.Label lblcpufreq;
        private System.Windows.Forms.Button btnctoff;
        private System.Windows.Forms.Button btncton;
        private System.Windows.Forms.Button btncfon;
        private System.Windows.Forms.Button btncfoff;
        private System.Windows.Forms.Button btncuoff;
        private System.Windows.Forms.Button btncuon;
        private System.Windows.Forms.Button btncpon;
        private System.Windows.Forms.Button btncpoff;
        private System.Windows.Forms.Label lblCPU;
        private System.Windows.Forms.Label lblRAM;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblramLoad;
        private System.Windows.Forms.Label lblramusage;
        private System.Windows.Forms.Button btnruon;
        private System.Windows.Forms.Button btnrlon;
        private System.Windows.Forms.Button btnrloff;
        private System.Windows.Forms.Button btnruoff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnglon;
        private System.Windows.Forms.Button btngloff;
        private System.Windows.Forms.Label lbltGPU;
        private System.Windows.Forms.Label lblpGPU;
        private System.Windows.Forms.Button btngpon;
        private System.Windows.Forms.Label lbllGPU;
        private System.Windows.Forms.Button btngton;
        private System.Windows.Forms.Button btngtoff;
        private System.Windows.Forms.Button btngpoff;
        private System.Windows.Forms.Button btnvramfoff;
        private System.Windows.Forms.Button btngfoff;
        private System.Windows.Forms.Button btnvramfon;
        private System.Windows.Forms.Label lblfVRAM;
        private System.Windows.Forms.Button btngfon;
        private System.Windows.Forms.Label lblfGPU;
        private System.Windows.Forms.Button btnCloseALL;
        private System.Windows.Forms.Timer timerData;
    }
}

