﻿using FWHWproject.Properties;
using System.Drawing;

namespace FWHWproject.lib
{
    static class Constants
    {   //BASE
        public const int min_base = 0; /*Base MIN val for all CircleProgressiveBar*/
        public const int max_loadPorcent = 100; /*Usage is always 100%*/
        public const string symbol_ = "\0";
        //CPU
        public const int max_tempCPU = 110; /*CPU Extremly MAX Temp*/
        public const int max_powerCPU = 300; /*CPU Extremly MAX Power <> I hope you don't have the Intel 22c/44t 300w beast XD*/
        public const int max_freqCPU = 6000; /*CPU SUPER Extremly MAX Frequency on CNTP*/
        //RAM
        public const int max_usageRAM = 100; /*RAM MAX, base on the total RAM active on system <> -1 works as flag*/
        public const int max_presentedRAM = 49152; /*MAX RAM presented on the machine*/ //I NEED C++ FOR THE DAMN POINTER!!!! FUCK YOU C#
        //GPU
        public const int max_tempGPU = 110; /*GPU Extremly MAX Temp*/
        public const int max_powerGPU = 450; /*GPU Extremly MAX Power*/
        public const int max_freqGPU = 2500; /*GPU Extremly MAX Freq*/
        public const int max_freqVRAM = 3000; /*VRAM Extremly MAX Freq*/
    }
    public struct data_mon
    {
        public string component_, symbol_;
        public Icon icon_;
        public int max, min, value_;
    }
    class hardwareSpecs
    {
        public hardwareSpecs() { }
        public data_mon monitoring_(string type_)
        {
            data_mon valores = new data_mon();
            valores.min = Constants.min_base;

            if (type_.ToLower().StartsWith("cpu"))       valores.component_ = "cpu";
            else if (type_.ToLower().StartsWith("gpu"))  valores.component_ = "gpu";
            else if (type_.ToLower().StartsWith("vram")) valores.component_ = "vram";
            else if (type_.ToLower().StartsWith("ram"))  valores.component_ = "ram";

            //SET MAX val depending on the hardware / chip
            switch (type_)
            {
                case "cpu_temp":
                    {
                        valores.max = Constants.max_tempCPU;
                        valores.symbol_ = "ºC";
                        valores.icon_ = Resources.cputemp;
                        break;
                    }
                case "cpu_power":
                    {
                        valores.max = Constants.max_powerCPU;
                        valores.symbol_ = "W";
                        valores.icon_ = Resources.cpupower;
                        break;
                    }
                case "cpu_load":
                    {
                        valores.max = Constants.max_loadPorcent;
                        valores.symbol_ = "%";
                        valores.icon_ = Resources.cpuload;
                        break;
                    }
                case "cpu_freq":
                    {
                        valores.max = Constants.max_freqCPU;
                        valores.symbol_ = "Mhz";
                        valores.icon_ = Resources.cpuclock;
                        break;
                    }
                case "ram_load":
                    {
                        valores.max = Constants.max_loadPorcent;
                        valores.symbol_ = "%";
                        valores.icon_ = Resources.ramload;
                        break;
                    }
                case "ram_usage":
                    {
                        valores.symbol_ = "Gb";
                        valores.icon_ = Resources.ramusage;
                        break;
                    }
                case "gpu_temp":
                    {
                        valores.max = Constants.max_tempGPU;
                        valores.symbol_ = "ºC";
                        valores.icon_ = Resources.gpu;
                        break;
                    }
                case "gpu_power":
                    {
                        valores.max = Constants.max_powerGPU;
                        valores.symbol_ = "W";
                        valores.icon_ = Resources.gpu;
                        break;
                    }
                case "gpu_load":
                    {
                        valores.max = Constants.max_loadPorcent;
                        valores.symbol_ = "%";
                        valores.icon_ = Resources.gpu;
                        break;
                    }
                case "gpu_freq":
                    {
                        valores.max = Constants.max_freqGPU;
                        valores.symbol_ = "Mhz";
                        valores.icon_ = Resources.gpu;
                        break;
                    }
                case "vram_freq":
                    {
                        valores.max = Constants.max_freqVRAM;
                        valores.symbol_ = "Mhz";
                        valores.icon_ = Resources.gpu;
                        break;
                    }
            } return valores;
        }
    }
}
