﻿using FWHWproject.lib;
using System;
using System.Windows.Forms;

namespace FWHWproject
{
    public partial class fTemCirc : Form
    {
        private string myType_ = null;
        public fTemCirc(string type_, data_mon misDatos)
        {
            InitializeComponent();
            this.myType_ = type_;
            this.Name = type_;
            this.Text = type_;
            this.Icon = misDatos.icon_;
            cpPlantilla.Text = misDatos.component_;
            cpPlantilla.Maximum = misDatos.max;
            cpPlantilla.Minimum = misDatos.min;
            cpPlantilla.SubscriptText = misDatos.symbol_;
        }
        public void set_value(float? in_)
        {
            cpPlantilla.Value = Convert.ToInt16(in_);
            cpPlantilla.SuperscriptText = in_?.ToString("0.0");
        }
    }
}
